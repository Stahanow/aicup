﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraffCore
{
    public interface INode { }

    public interface IState {
        void OnInter();
        void OnEsc();
    }

    public interface IPredicate {
        bool Value(IState x);
    }

    public interface IKeyProvider<T> : INode {
        T Key { get; }
    }
}
