﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GraffCore;

namespace GraffCore
{
    public partial class StateMachine
    {
        Graff<IState, string> graff;
        IState startState;
        IState endState;
        IState currentState;

        public IState StartState { get { return startState; } set { startState = value; } }
        public IState EndState { get { return endState; } set { endState = value; } }
        public IState CurrentState { get { return currentState; } set { currentState = value; } }

        public StateMachine(Graff<IState, string> graff)
        {
            this.graff = graff;
        }
        public void NextState()
        {
            IState newState = GetNextState();

            if (newState != currentState && currentState != null)
                currentState.OnEsc();

            newState.OnInter();

            currentState = newState;
        }
        public void Reset()
        {
            currentState = startState;
        }

        IState GetNextState()
        {
            if (currentState == endState && endState != null)
                return currentState;
            //List<IState> list = graff.GetNeighbors(currentState);
            List<IEdge<IState>> edges = graff.GetEdges(currentState).FindAll((x) => { return x.ANode.Equals(currentState); });
            foreach (IEdge<IState> edge in edges)
            {
                IPredicate pr = edge as IPredicate;
                if (pr != null && pr.Value(currentState))
                    return edge[currentState];
            }
            return currentState;
            //IPredicate currentPredicate = currentState as IPredicate;
            //if (currentPredicate == null)
            //    return list[0];
            //foreach (IState state in list)
            //    if (currentPredicate.Value(state))
            //        return state;
            //return currentState;
        }
    }
}
