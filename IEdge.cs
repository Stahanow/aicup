﻿using System;
using System.Collections.Generic;

namespace GraffCore
{    
    public interface IEdge<TNode> where TNode : class 
    {
        TNode ANode { get; }
        TNode BNode { get; }

        bool IsUnite(TNode x, TNode y);
        TNode this[TNode x] { get; }
    }
}
