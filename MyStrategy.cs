using System;
using System.Collections.Generic;
using Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk.Model;
using GraffCore;

namespace Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk
{
    public sealed class MyStrategy : IStrategy
    {
        public Point _center;

        public World _world;
        public Hockeyist _self;
        public Game _game;
        public int swingCount;
        Point speed;
        Graff<IState, string> graff;
        StateMachine stateMachine;
        IState previousStrategy;

        const double strikeFieldOk = -1.0;
        const double strikeSpeed = 2.9;
        const int swingMax = 15;
        const double speedUpInStrikeZone = -1;
        const double EncircleRadius = 70;
        const double DistanceToPass = 250;
        Strategy find;

        public MyStrategy()
        {
            graff = new Graff<IState, string>();
            find = new Find(this);
            Strategy protect = new Protect(this);
            Strategy attack = new Attack(this);
            Strategy raid = new Raid(this);
            Strategy attackAssistant = new AttackAssistant(this);
            Strategy pass = new Pass(this);
            Strategy brutalAttack = new BrutalAttack(this);
            Strategy ambush = new Ambush(this);

            graff.AddNode(find, find.Name);
            graff.AddNode(protect, protect.Name);
            graff.AddNode(attack, attack.Name);
            graff.AddNode(brutalAttack, brutalAttack.Name);
            graff.AddNode(raid, raid.Name);
            graff.AddNode(attackAssistant, attackAssistant.Name);
            graff.AddNode(pass, pass.Name);
            graff.AddNode(ambush, ambush.Name);

            Change findToRaid = new Change(find, raid, PuckOnRivals, this);
            Change findToProtect = new Change(find, protect, (w, s) => { return PuckOnRivals(w) && INearestToGate(s, w) && !IsStrategyExist(typeof(Protect)); }, this);
            Change findToAttack = new Change(find, attack, PuckIsMine, this);
            //Change findToAttackAssistant = new Change(find, attackAssistant, (w, s) => { return PuckIsOur(w) && !PuckIsMine(w, s); }, this);
            Change findToAmbush = new Change(find, ambush, (w, s) => { return PuckIsOur(w) && !PuckIsMine(w, s); }, this);

            Change protectToFind = new Change(protect, find, PuckIsMine, this);
            Change protectToFind1 = new Change(protect, find, (w, s) => { return PuckIsFree(w) && w.Puck.GetDistanceTo(s) < 120 && DistanceToNearestRival(w, s) > 700; }, this);
            Change protectToAttack = new Change(protect, attack, PuckIsMine, this);

            Change raidToFind = new Change(raid, find, (w) => { return !PuckOnRivals(w); }, this);
            Change raidToAttack = new Change(raid, attack, PuckIsMine, this);

            Change attackToFind = new Change(attack, find, (w, s) => { return !PuckIsMine(w, s); }, this);
            Change attackToPass = new Change(attack, pass, (w, s) => { return IAmEncircle(w, s) || IInBedPoint(w, s); }, this);
            Change attackToBrutalAttack = new Change(attack, brutalAttack, (w) => { return GetRivalGoalie(w) == null; }, this);

            Change brutalAttackToFind = new Change(brutalAttack, find, (w, s) => { return !PuckIsMine(w, s); }, this);

            //Change attackToPass = new Change(attack, pass, IInBedPoint, this);

            Change passToFind = new Change(pass, find, (w, s) => { return !PuckIsMine(w, s); }, this);

            //Change attackAssistantToFind = new Change(attackAssistant, find, (w) => { return !PuckIsOur(w); }, this);
            Change ambushToFind = new Change(ambush, find, (w) => { return !PuckIsOur(w); }, this);

            graff.AddEdge(findToProtect);
            graff.AddEdge(findToRaid);
            graff.AddEdge(findToAttack);
            //graff.AddEdge(findToAttackAssistant);
            graff.AddEdge(findToAmbush);

            graff.AddEdge(protectToFind);
            graff.AddEdge(protectToFind1);
            graff.AddEdge(protectToAttack);

            graff.AddEdge(raidToFind);
            graff.AddEdge(raidToAttack);

            graff.AddEdge(attackToFind);
            graff.AddEdge(attackToPass);
            graff.AddEdge(attackToBrutalAttack);

            graff.AddEdge(brutalAttackToFind);

            graff.AddEdge(passToFind);

            //graff.AddEdge(attackAssistantToFind);
            graff.AddEdge(ambushToFind);

            stateMachine = new StateMachine(graff);
            stateMachine.StartState = find;
            stateMachine.Reset();
            //previousStrategy = find;
        }

        public void Move(Hockeyist self, World world, Game game, Move move)
        {
            Singleton.Instance.Add(self, stateMachine);

            this._self = self;
            this.speed = new Point(self.SpeedX, self.SpeedY);
            this._world = world;
            this._game = game;
            this._center = new Point(game.WorldWidth / 2, game.WorldHeight / 2);


            if (self.LastAction.HasValue && self.LastAction.Value == ActionType.Swing && !PuckIsMine(world, self))
            {
                move.Action = ActionType.CancelStrike;
                swingCount = 0;
                stateMachine.CurrentState = find;
            }
            do
            {
                previousStrategy = stateMachine.CurrentState;
                stateMachine.NextState();
            }
            while (previousStrategy != stateMachine.CurrentState);

            Strategy strategy = stateMachine.CurrentState as Strategy;
            strategy.Run(move);

            /*
            if (PuckIsOur(world))
            {
                if (InStrikeZone(self) || WeAreCloser(world) || swingCount > 0)
                {
                    //if (GetSpead(self) < 5)
                    AimAndStrike(self, world, game, move);
                    //else
                    //    move.SpeedUp = -1.0;
                }
                else if (Following(self))
                {// &&
                    //game.StickLength > GetNearestRival(world, self).GetDistanceTo(self) ) {
                    //Hockeyist rival = GetNearestRival(world, self);
                    Hockeyist rival = GetNearestRival(world, GetById(world.Puck.OwnerHockeyistId));
                    double angl = self.GetAngleTo(rival);
                    if (angl < Math.PI / 60 && game.StickLength > rival.GetDistanceTo(self))
                        move.Action = ActionType.Strike;
                    else
                        move.Turn = angl;
                    move.SpeedUp = 1.0;
                }
                else
                {
                    double strikeField = strikePointsField(Point.Get(self));
                    Point point = AngleToStrikeZone(self, world);
                    //Point vector = new Point(point.X - self.X, point.Y - self.Y);
                    double angl = self.GetAngleTo(point.X, point.Y);

                    //move.SpeedUp += 1 / (1 + Math.Exp(-dis - 5)) - 1;
                    //move.SpeedUp += 2 / (1 + Math.Exp(-dis + 60));
                    move.SpeedUp = 1;// / (1 + Math.Exp(-dis + 60));

                    if (strikeField > strikeFieldOk * 1.2 && angl < Math.PI / 70)
                        move.SpeedUp = speedUpInStrikeZone;

                    move.Turn = angl;
                    if (self.LastAction.HasValue && self.LastAction.Value == ActionType.Swing)
                    {
                        move.Action = ActionType.CancelStrike;
                        swingCount = 0;
                    }
                }
            }
            if (PuckOnRivals(world))
            {
                if (self.LastAction.HasValue && self.LastAction.Value == ActionType.Swing)
                {
                    move.Action = ActionType.CancelStrike;
                    swingCount = 0;
                }
                Hockeyist rivalWithPuck = GetById(world.Puck.OwnerHockeyistId);
                if (game.StickLength > rivalWithPuck.GetDistanceTo(self))
                {
                    double angl = self.GetAngleTo(rivalWithPuck);
                    if (angl < Math.PI / 70)
                        move.Action = ActionType.Strike;
                    else
                        move.Turn = angl;
                }
                else if (game.StickLength > self.GetDistanceTo(world.Puck))
                {
                    double angl = self.GetAngleTo(world.Puck);
                    if (angl < Math.PI / 70)
                        move.Action = ActionType.Strike;
                    else
                        move.Turn = angl;
                }
                else
                {
                    Hockeyist h = GetNearestTeam(world, rivalWithPuck);
                    double x = rivalWithPuck.X + rivalWithPuck.SpeedX * 5;
                    double y = rivalWithPuck.Y + rivalWithPuck.SpeedY * 5;
                    move.Turn = self.GetAngleTo(x, y);
                    if (h.Id == self.Id || _center.GetDistanceTo(self) > 100)
                        move.SpeedUp = 1.0;
                    else if (GetSpead(self) > 2)
                        move.SpeedUp = -1.0;
                }
            }
            if (PuckIsFree(world))
            {
                if (self.LastAction.HasValue && self.LastAction.Value == ActionType.Swing)
                {
                    move.Action = ActionType.CancelStrike;
                    swingCount = 0;
                }
                if (HockeyistNextToThePuck(self))
                {
                    move.Action = ActionType.TakePuck;
                    move.SpeedUp = 1.0;
                }
                else
                {
                    Hockeyist h = GetNearestTeam(world, world.Puck);
                    double x = world.Puck.X + world.Puck.SpeedX * 5;
                    double y = world.Puck.Y + world.Puck.SpeedY * 5;
                    move.Turn = self.GetAngleTo(x, y);
                    if (h.Id == self.Id || _center.GetDistanceTo(self) > 200)
                        move.SpeedUp = 1.0;
                    else if (GetSpead(self) > 2)
                        move.SpeedUp = -1.0;
                }
            }*/
        }

        void AimAndStrike(Hockeyist self, World world, Game game, Move move)
        {
            Point goalPoint;

            Point goalNetLeft = new Point(world.GetOpponentPlayer().NetRight, world.GetOpponentPlayer().NetTop);
            Point goalNetRight = new Point(world.GetOpponentPlayer().NetRight, world.GetOpponentPlayer().NetTop + game.GoalNetHeight);

            Hockeyist goalie = GetGoalie(world.Hockeyists, world.GetMyPlayer());
            if (goalie != null)
            {
                double distTogoalNetLeft = goalie.GetDistanceTo(goalNetLeft.X, goalNetLeft.Y);
                double distTogoalNetRight = goalie.GetDistanceTo(goalNetRight.X, goalNetRight.Y);

                if (distTogoalNetLeft > distTogoalNetRight)
                    goalPoint = goalNetLeft;
                else
                    goalPoint = goalNetRight;
            }
            else
                goalPoint = GetRivalGoalNetCenter(world);

            double angl = self.GetAngleTo(goalPoint.X, goalPoint.Y);

            if (Math.Abs(angl) < Math.PI / 60 && GetSpead(self) > strikeSpeed)
            {
                move.Action = ActionType.Strike;
                swingCount = 0;
            }
            else if (Math.Abs(angl) < Math.PI / 60 || swingCount > 0)
            {
                if (swingCount < swingMax)
                {
                    move.Action = ActionType.Swing;
                    swingCount++;
                }
                else
                {
                    move.Action = ActionType.Strike;
                    swingCount = 0;
                }
            }
            else
            {
                swingCount = 0;
                move.Action = ActionType.CancelStrike;
                move.Turn = angl;
            }
        }

        bool INearestToGate(Hockeyist self, World world)
        {
            Hockeyist nearest = GetNearest(GetMyTeam(world), GetMyGoalNetCenter(world));
            return nearest.Id == self.Id;
        }

        bool IsStrategyExist(Type t)
        {
            return Singleton.Instance.IsExist(t);
        }

        bool IAmEncircle(World world, Hockeyist self)
        {
            List<Hockeyist> h = GetRival(world).FindAll((x) => { return x.GetDistanceTo(self) < EncircleRadius; });
            return h.Count >= 2;
        }

        bool IInBedPoint(World world, Hockeyist self)
        {
            List<Hockeyist> team = GetMyTeam(world).FindAll((x) => { return x.Id != self.Id && Singleton.Instance[x] != null && !Singleton.Instance[x].CurrentState.GetType().Equals(typeof(Protect)); });
            //List<Hockeyist> team = GetMyTeam(world).FindAll((x) => { return x.Id != self.Id; });
            if (team.Count == 0)
                return false;
            Hockeyist h = team[0];
            double myF = mainField(world, Point.Get(self), world.GetMyPlayer());
            double hF = mainField(world, Point.Get(h), world.GetMyPlayer());
            return myF * 2 < hF && self.GetDistanceTo(h) > DistanceToPass;
        }

        bool PuckIsFree(World world)
        {
            return world.Puck.OwnerPlayerId != world.GetMyPlayer().Id && world.Puck.OwnerPlayerId != world.GetOpponentPlayer().Id;
        }

        bool PuckOnRivals(World world)
        {
            return world.Puck.OwnerPlayerId == world.GetOpponentPlayer().Id;
        }

        bool PuckIsOur(World world)
        {
            return world.Puck.OwnerPlayerId == world.GetMyPlayer().Id;
        }

        bool PuckIsMine(World world, Hockeyist self)
        {
            return world.Puck.OwnerHockeyistId == self.Id;
        }

        bool Following(Hockeyist self)
        {
            return _world.Puck.OwnerPlayerId == _world.GetMyPlayer().Id && _world.Puck.OwnerHockeyistId != self.Id;
        }

        bool InStrikeZone(Hockeyist self)
        {
            //return (GoalNetDistance(self.X, self.Y, world) < 0.36 && world.Puck.OwnerHockeyistId == self.Id);
            return (strikePointsField(Point.Get(self)) > strikeFieldOk && _world.Puck.OwnerHockeyistId == self.Id &&
                (self.Y > _world.GetOpponentPlayer().NetTop || self.Y < _world.GetOpponentPlayer().NetBottom));
            //else if ((strikePointsField(Point.Get(self)) > 0.8 && world.Puck.OwnerHockeyistId == self.Id) ||
        }

        double GetSpead(Unit u)
        {
            return Math.Sqrt(u.SpeedX * u.SpeedX + u.SpeedY * u.SpeedY);
        }

        bool HockeyistNextToThePuck(Hockeyist self)
        {
            return _game.StickLength > self.GetDistanceTo(_world.Puck) && _world.Puck.OwnerHockeyistId != self.Id && _world.Puck.OwnerPlayerId != _world.GetMyPlayer().Id;
        }

        Hockeyist GetGoalie(Hockeyist[] hockeyists, Player me)
        {
            foreach (Hockeyist h in hockeyists)
                if (h.Type == HockeyistType.Goalie && h.PlayerId != me.Id)
                    return h;
            return null;
        }

        Hockeyist GetById(long id)
        {
            foreach (Hockeyist h in _world.Hockeyists)
                if (h.Id == id)
                    return h;
            return null;
        }

        bool WeAreCloser(World world)
        {
            Point goalNet = GetRivalGoalNetCenter(world);

            List<Hockeyist> myTeam = GetMyTeam(world);

            return goalNet.GetDistanceTo(GetNearest(GetRival(world), goalNet)) > goalNet.GetDistanceTo(GetNearest(myTeam, goalNet)) && AllInCercal(myTeam, GetMyGoalNetCenter(world), 120);
        }

        bool AllInCercal(List<Hockeyist> list, Point point, double r)
        {
            foreach (Hockeyist h in list)
            {
                if (point.GetDistanceTo(h) > r)
                    return false;
            }
            return true;
        }

        Hockeyist ManWithPuck(World world)
        {
            return GetById(world.Puck.OwnerHockeyistId);
        }

        Hockeyist GetNearestRival(World world, Unit unit)
        {
            Player me = world.GetMyPlayer();
            Hockeyist rival = null;
            double d = double.MaxValue;
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    double dis = h.GetDistanceTo(unit);
                    if (dis < d)
                    {
                        d = dis;
                        rival = h;
                    }
                }
            return rival;
        }

        double DistanceToNearestRival(World world, Unit unit)
        {
            return GetNearestRival(world, unit).GetDistanceTo(unit);
        }

        Hockeyist GetNearest(List<Hockeyist> list, Point point)
        {
            Hockeyist hockeyist = null;
            double d = double.MaxValue;
            foreach (Hockeyist h in list)
            {
                double dis = point.GetDistanceTo(h);
                if (dis < d)
                {
                    d = dis;
                    hockeyist = h;
                }
            }
            return hockeyist;
        }

        List<Hockeyist> GetRival(World world)
        {
            List<Hockeyist> list = new List<Hockeyist>();
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    list.Add(h);
                }
            return list;
        }

        List<Hockeyist> GetMyTeam(World world)
        {
            List<Hockeyist> list = new List<Hockeyist>();
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId == me.Id)
                {
                    list.Add(h);
                }
            return list;
        }

        Hockeyist GetRivalGoalie(World world)
        {
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type == HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    return h;
                }
            return null;
        }

        Hockeyist GetNearestTeam(World world, Unit unit)
        {
            Player me = world.GetMyPlayer();
            Hockeyist rival = null;
            double d = double.MaxValue;
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId == me.Id)
                {
                    double dis = h.GetDistanceTo(unit);
                    if (dis < d)
                    {
                        d = dis;
                        rival = h;
                    }
                }
            return rival;
        }

        Point GetRivalGoalNetCenter(World world)
        {
            return new Point(world.GetOpponentPlayer().NetRight, (world.GetOpponentPlayer().NetBottom + world.GetOpponentPlayer().NetTop) / 2);
        }
        Point GetMyGoalNetCenter(World world)
        {
            return new Point(world.GetMyPlayer().NetRight, (world.GetMyPlayer().NetBottom + world.GetMyPlayer().NetTop) / 2);
        }

        Point AngleToStrikeZone(Hockeyist self, World world)
        {
            //Hockeyist goalie = GetGoalie(world.Hockeyists, world.GetMyPlayer());
            //if (goalie != null)
            //    move.Turn = self.GetAngleTo(goalie);
            //else {
            //    Point p = GetGoalNetCenter(world);
            //    move.Turn = self.GetAngleTo(p.X, p.Y);
            //}

            List<Point> points = GetNextPoints(self, world);
            Point dist = points[2];
            double distValue = double.MinValue;
            foreach (Point p in points)
            {
                double value = mainField(world, p, world.GetMyPlayer());
                if (value > distValue)
                {
                    distValue = value;
                    dist = p;
                }
            }
            return dist;//self.GetAngleTo(dist.X, dist.Y);
        }

        List<Point> GetNextPoints(Hockeyist self, World world)
        {
            List<Point> points = new List<Point>();
            Point spead = new Point(self.Radius, 0);
            Point selfPoint = new Point(self.X, self.Y);
            for (double ang = 0; ang < 360; ang += 15.0)
                points.Add(selfPoint + RotateVector(spead, ang));
            return points;
        }

        Point RotateVector(Point v, double angl)
        {
            return new Point(v.X * Math.Cos(angl) - v.Y * Math.Sin(angl), v.X * Math.Sin(angl) + v.Y * Math.Cos(angl));
        }

        double rivalsField(Hockeyist[] hockeyists, Point point, Player me)
        {
            double field = 0;
            foreach (Hockeyist h in hockeyists)
                if (h.PlayerId != me.Id)
                {
                    double dis = h.GetDistanceTo(point.X, point.Y);
                    field += (-3 / (1 + Math.Exp(0.08 * dis - 4.5)));
                }
            return field;
        }

        double strikePointsField(Point point)
        {
            Point goalNet = GetRivalGoalNetCenter(_world);
            Point topStrike;
            Point bottomStrike;

            double heightFactor = 0.2;
            double widthFactor = 0.33;

            if (IsRivalsAtLeft)
            {
                topStrike = new Point(goalNet.X + _game.WorldWidth * widthFactor, goalNet.Y + _game.WorldHeight * heightFactor);
                bottomStrike = new Point(goalNet.X + _game.WorldWidth * widthFactor, goalNet.Y - _game.WorldHeight * heightFactor);
            }
            else
            {
                topStrike = new Point(goalNet.X - _game.WorldWidth * widthFactor, goalNet.Y + _game.WorldHeight * heightFactor);
                bottomStrike = new Point(goalNet.X - _game.WorldWidth * widthFactor, goalNet.Y - _game.WorldHeight * heightFactor);
            }

            return Math.Max(1 - topStrike.GetDistanceTo(point) * 0.02, 1 - bottomStrike.GetDistanceTo(point) * 0.02);
        }

        double mainField(World world, Point point, Player me)
        {
            //double field = 0;

            //Hockeyist goalie = GetGoalie(world.Hockeyists, world.GetMyPlayer());
            //if (goalie != null) {
            //    field += 10 - goalie.GetDistanceTo(point.X, point.Y) * 0.2;
            //}
            //else {
            //    Point p = GetRivalGoalNetCenter(world);
            //    field += 10 - p.GetDistanceTo(point);
            //}            

            return strikePointsField(point) + rivalsField(world.Hockeyists, point, me);
        }

        bool IsRivalsAtLeft
        {
            get
            {
                Point p = GetRivalGoalNetCenter(_world);
                return p.X < this._game.WorldWidth / 2;
            }
        }

        double GoalNetDistance(double x, double y, World world)
        {

            double ngX = world.GetOpponentPlayer().NetRight;
            return Math.Abs(ngX - x) / world.Width;
            //double ngX = world.GetOpponentPlayer().NetRight;
            //double ngY = (world.GetOpponentPlayer().NetTop + world.GetOpponentPlayer().NetTop) / 2;

            //double d = Math.Sqrt((x - ngX) * (x - ngX) + (y - ngY) * (y - ngY));
            //double dp = d / world.Width;
            //return dp;
        }
    }



    public class Singleton
    {
        Dictionary<Hockeyist, StateMachine> machines;
        private static Singleton instance;
        Point ambushPoint;

        private Singleton()
        {
            machines = new Dictionary<Hockeyist, StateMachine>();
        }

        public void Add(Hockeyist h, StateMachine m)
        {
            if (!machines.ContainsKey(h))
                machines.Add(h, m);
        }

        public StateMachine this[Hockeyist h]
        {
            get
            {
                if (machines.ContainsKey(h))
                    return machines[h];
                return null;
            }
        }

        public bool IsExist(Type t)
        {

            foreach (StateMachine m in machines.Values)
                if (m.CurrentState != null && m.CurrentState.GetType().Equals(t))
                    return true;
            return false;
        }
        public Point AmbushPoint
        {
            get
            {
                return ambushPoint;
            }
            set
            {
                ambushPoint = value;
            }
        }

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }
    }

    public class Point
    {
        double x;
        double y;
        public double X { get { return x; } }
        public double Y { get { return y; } }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public double GetDistanceTo(Point a)
        {
            return Math.Sqrt((this.X - a.X) * (this.X - a.X) + (this.Y - a.Y) * (this.Y - a.Y));
        }

        public double GetDistanceTo(Unit u)
        {
            return u.GetDistanceTo(this.X, this.Y);
        }

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public static Point Get(Hockeyist self)
        {
            return new Point(self.X, self.Y);
        }

        public static Point GetNearest(List<Point> list, Point p)
        {
            Point nearestPoint = null;
            double d = double.MaxValue;
            foreach (Point point in list)
            {
                double dis = point.GetDistanceTo(p);
                if (dis < d)
                {
                    d = dis;
                    nearestPoint = point;
                }
            }
            return nearestPoint;
        }

        public static Point GetFarther(List<Point> list, Point p)
        {
            Point fartherPoint = null;
            double d = double.MinValue;
            foreach (Point point in list)
            {
                double dis = point.GetDistanceTo(p);
                if (dis > d)
                {
                    d = dis;
                    fartherPoint = point;
                }
            }
            return fartherPoint;
        }
    }

    public class Vector
    {
        double x;
        double y;
        public double X { get { return x; } }
        public double Y { get { return y; } }

        public static double operator *(Vector a, Vector b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static Point operator +(Point a, Vector b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public Vector Rotate(double angl)
        {
            return new Vector(X * Math.Cos(angl) - Y * Math.Sin(angl), X * Math.Sin(angl) + Y * Math.Cos(angl));
        }

        public double Abs()
        {
            return Math.Sqrt(x * x + y * y);
        }

        public Vector UnitVector()
        {
            double abs = Abs();
            return new Vector(X / abs, Y / abs);
        }

        public static Point GetSpeed(Unit un)
        {
            return new Point(un.SpeedX, un.SpeedY);
        }

        public Vector(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}