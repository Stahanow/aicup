﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraffCore
{
    public class Edge<TNode> : IEdge<TNode> where TNode : class 
    {
        readonly TNode aNode;
        readonly TNode bNode;

        public TNode ANode { get { return aNode; } }
        public TNode BNode { get { return bNode; } }

        public Edge(TNode aNode, TNode bNode)
        {
            this.aNode = aNode;
            this.bNode = bNode;
        }
        public bool IsUnite(TNode x, TNode y)
        {
            return (x.Equals(aNode) && y.Equals(bNode)) || (y.Equals(aNode) && x.Equals(bNode));
        }
        public TNode this[TNode x]
        {
            get
            {
                if (aNode.Equals(x))
                    return bNode;
                if (bNode.Equals(x))
                    return aNode;
                return null;
            }
        }
    }

    public class OrientedEdge<TNode> : IEdge<TNode> where TNode : class 
    {
        readonly TNode start;
        readonly TNode end;

        public TNode ANode { get { return start; } }
        public TNode BNode { get { return end; } }

        public OrientedEdge(TNode start, TNode end)
        {
            this.start = start;
            this.end = end;
        }
        public bool IsUnite(TNode x, TNode y)
        {
            return (x.Equals(start) && y.Equals(end));
        }
        public TNode this[TNode x]
        {
            get
            {
                if (start.Equals(x))
                    return end;
                if (end.Equals(x))
                    return start;
                return null;
            }
        }
    }
}
