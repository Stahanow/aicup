﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GraffCore.Tests
{
    public class NameNode : IKeyProvider<string>
    {
        string name;
        public string Key
        {
            get { return name; }
        }

        public NameNode(string name)
        {
            this.name = name;
        }
    }
}
