﻿using System;
using System.Collections.Generic;
using Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk.Model;
using GraffCore;

namespace Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk
{
    public class Change : IEdge<IState>, IPredicate
    {
        MyStrategy info;
        IState strategyA;
        IState strategyB;

        Func<World, Hockeyist, bool> predicate1;
        Func<World, bool> predicate2;

        public Change(Strategy strategyA, Strategy strategyB, Func<World, Hockeyist, bool> predicate, MyStrategy info)
        {
            this.strategyA = strategyA;
            this.strategyB = strategyB;
            this.predicate1 = predicate;
            this.info = info;
        }

        public Change(Strategy strategyA, Strategy strategyB, Func<World, bool> predicate, MyStrategy info)
        {
            this.strategyA = strategyA;
            this.strategyB = strategyB;
            this.predicate2 = predicate;
            this.info = info;
        }

        IState IEdge<IState>.ANode
        {
            get { return strategyA; }
        }

        IState IEdge<IState>.BNode
        {
            get { return strategyB; }
        }

        bool IEdge<IState>.IsUnite(IState x, IState y)
        {
            return x.Equals(strategyA) && y.Equals(strategyB);
        }

        IState IEdge<IState>.this[IState x]
        {
            get
            {
                if (strategyA.Equals(x))
                    return strategyB;
                if (strategyB.Equals(x))
                    return strategyA;
                return null;
            }
        }

        bool IPredicate.Value(IState x)
        {
            if (predicate1 != null)
                return predicate1(info._world, info._self);
            return predicate2(info._world);
        }

        public override string ToString()
        {
            return strategyA.ToString() + " to " + strategyB.ToString();
        }
    }
}
