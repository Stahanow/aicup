﻿using System;
using System.Collections.Generic;
using Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk.Model;
using GraffCore;

namespace Com.CodeGame.CodeHockey2014.DevKit.CSharpCgdk
{
    public abstract class Strategy : IState
    {
        protected World world { get { return info._world; } }
        protected Game game { get { return info._game; } }
        protected Hockeyist self { get { return info._self; } }
        protected Point center { get { return info._center; } }
        protected MyStrategy info;

        public Strategy(MyStrategy info)
        {
            this.info = info;
        }

        public abstract void Run(Move move);

        protected Hockeyist GetById(long id)
        {
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Id == id)
                    return h;
            return null;
        }

        protected List<Hockeyist> GetMyTeam(World world)
        {
            List<Hockeyist> list = new List<Hockeyist>();
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId == me.Id)
                {
                    list.Add(h);
                }
            return list;
        }

        protected double GetSpead(Unit u)
        {
            return Math.Sqrt(u.SpeedX * u.SpeedX + u.SpeedY * u.SpeedY);
        }

        protected bool PuckIsMine()
        {
            return world.Puck.OwnerPlayerId == world.GetMyPlayer().Id;
        }

        protected Hockeyist GetNearestTeam(Unit unit)
        {
            Player me = world.GetMyPlayer();
            Hockeyist rival = null;
            double d = double.MaxValue;
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId == me.Id)
                {
                    double dis = h.GetDistanceTo(unit);
                    if (dis < d)
                    {
                        d = dis;
                        rival = h;
                    }
                }
            return rival;
        }

        protected bool HockeyistNextToThePuck(Hockeyist self)
        {
            return game.StickLength > self.GetDistanceTo(world.Puck) && world.Puck.OwnerHockeyistId != self.Id && world.Puck.OwnerPlayerId != world.GetMyPlayer().Id;
        }

        protected bool PuckOnRivals()
        {
            return world.Puck.OwnerPlayerId == world.GetOpponentPlayer().Id;
        }

        protected Puck Puck { get { return world.Puck; } }

        protected Point GetMyGoalNetCenter()
        {
            if (IsRivalsAtLeft)
                return new Point(world.GetMyPlayer().NetLeft, (world.GetMyPlayer().NetBottom + world.GetMyPlayer().NetTop) / 2);
            return new Point(world.GetMyPlayer().NetRight, (world.GetMyPlayer().NetBottom + world.GetMyPlayer().NetTop) / 2);
        }
        protected Point GetRivalGoalNetCenter()
        {
            return new Point(world.GetOpponentPlayer().NetRight, (world.GetOpponentPlayer().NetBottom + world.GetOpponentPlayer().NetTop) / 2);
        }

        protected bool OnMyPartOfFild(Unit unit)
        {
            return GetMyGoalNetCenter().GetDistanceTo(unit) < world.Width / 2;
        }

        protected Point AngleToZone(Hockeyist self, Func<Point, double> field)
        {
            List<Point> points = GetPointsAround(self);
            Point dist = points[0];
            double distValue = double.MinValue;
            foreach (Point p in points)
            {
                double value = field(p);
                if (value > distValue)
                {
                    distValue = value;
                    dist = p;
                }
            }
            return dist;
        }

        protected List<Point> GetStrikePoints()
        {
            Point goalNet = GetRivalGoalNetCenter();
            Point topStrike;
            Point bottomStrike;

            double heightFactor = 0.25;
            double widthFactor = 0.33;

            if (IsRivalsAtLeft)
            {
                topStrike = new Point(goalNet.X + game.WorldWidth * widthFactor, goalNet.Y + game.WorldHeight * heightFactor);
                bottomStrike = new Point(goalNet.X + game.WorldWidth * widthFactor, goalNet.Y - game.WorldHeight * heightFactor);
            }
            else
            {
                topStrike = new Point(goalNet.X - game.WorldWidth * widthFactor, goalNet.Y + game.WorldHeight * heightFactor);
                bottomStrike = new Point(goalNet.X - game.WorldWidth * widthFactor, goalNet.Y - game.WorldHeight * heightFactor);
            }
            List<Point> list = new List<Point>();
            list.Add(topStrike);
            list.Add(bottomStrike);
            return list;
        }

        protected Point GetNextCoordinates(Unit un)
        {
            return new Point(un.X + un.SpeedX, un.Y + un.SpeedY);
        }

        protected bool InActionZone(Unit un)
        {
            return self.GetDistanceTo(un) < game.StickLength;
        }

        protected bool IsRivalsAtLeft
        {
            get
            {
                Point p = GetRivalGoalNetCenter();
                return p.X < this.game.WorldWidth / 2;
            }
        }

        protected Hockeyist GetNearestRival(World world, Unit unit)
        {
            Player me = world.GetMyPlayer();
            Hockeyist rival = null;
            double d = double.MaxValue;
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    double dis = h.GetDistanceTo(unit);
                    if (dis < d)
                    {
                        d = dis;
                        rival = h;
                    }
                }
            return rival;
        }

        protected List<Point> GetPointsAround(Unit self)
        {
            List<Point> points = new List<Point>();
            Vector spead = new Vector(self.Radius, 0);
            Point selfPoint = new Point(self.X, self.Y);
            for (double ang = 0; ang < 360; ang += 10.0)
                points.Add(selfPoint + spead.Rotate(ang));
            return points;
        }

        protected double rivalsField(Hockeyist[] hockeyists, Point point, Player me)
        {
            double field = 0;
            foreach (Hockeyist h in hockeyists)
                if (h.PlayerId != me.Id)
                {
                    double dis = h.GetDistanceTo(point.X, point.Y);
                    field += (-3 / (1 + Math.Exp(0.08 * dis - 4.5)));
                }
            return field;
        }

        public abstract string Name { get; }
        public override string ToString()
        {
            return Name;
        }

        void IState.OnInter()
        {
            OnInter();
        }
        void IState.OnEsc()
        {
            OnEsc();
        }

        protected virtual void OnInter() { }
        protected virtual void OnEsc() { }

    }

    public class Find : Strategy
    {
        public override string Name { get { return "find"; } }

        public Find(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            if (HockeyistNextToThePuck(self))
            {
                move.Action = ActionType.TakePuck;
                move.SpeedUp = 1.0;
            }
            else
            {
                double x = world.Puck.X + world.Puck.SpeedX * 5;
                double y = world.Puck.Y + world.Puck.SpeedY * 5;

                double angl = self.GetAngleTo(x, y);

                Vector myVector = new Vector(1, 0).Rotate(self.Angle);
                Vector toStrikeZone = new Vector(x - self.X, y - self.Y);
                double projection = myVector * toStrikeZone / toStrikeZone.Abs();

                if (projection > 0.0)
                {
                    move.SpeedUp = 1.0;
                    move.Turn = angl;
                }
                else if (projection < -0.8)
                {
                    move.SpeedUp = -1.0;
                    move.Turn = angl;
                }
                else
                {
                    move.SpeedUp = 0.0;
                    move.Turn = angl;
                }

                //if (h.Id == self.Id || center.GetDistanceTo(self) > 200)
                //    move.SpeedUp = 1.0;
                //else if (GetSpead(self) > 2)
                //    move.SpeedUp = -1.0;
            }
        }

        bool PuckIsFree()
        {
            return world.Puck.OwnerPlayerId != world.GetMyPlayer().Id && world.Puck.OwnerPlayerId != world.GetOpponentPlayer().Id;
        }
    }

    public class Protect : Strategy
    {
        public override string Name { get { return "Protect"; } }

        public Protect(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            double field = ProtectZone(GetNextCoordinates(self));
            if (field > 0.0)
            {
                if (GetSpead(self) > 1)
                    move.SpeedUp = 0.0;
                else
                {
                    move.Turn = self.GetAngleTo(Puck);
                    if (InActionZone(Puck))
                    {
                        if (GetSpead(Puck) < 8)
                            move.Action = ActionType.TakePuck;
                        else
                            move.Action = ActionType.Strike;
                    }
                }
            }
            else if (HockeyistNextToThePuck(self))
            {
                move.Action = ActionType.TakePuck;
                move.SpeedUp = 1.0;
            }
            else
            {
                Hockeyist rival = GetNearestRival(world, self);
                double angl = self.GetAngleTo(rival);
                if (angl < game.StrikeAngleDeviation && game.StickLength > rival.GetDistanceTo(self))
                    move.Action = ActionType.Strike;
                else
                {
                    Point point = AngleToProtectZone(self, ProtectZone);
                    Vector myVector = new Vector(1, 0).Rotate(self.Angle);
                    Vector toProtectZone = new Vector(point.X - self.X, point.Y - self.Y);
                    double projection = myVector * toProtectZone / toProtectZone.Abs();
                    angl = self.GetAngleTo(point.X, point.Y);

                    if (projection > 0.0)
                    {
                        move.SpeedUp = 1.0;
                        move.Turn = angl;
                    }
                    else if (projection < -0.8)
                    {
                        move.SpeedUp = -1.0;
                        move.Turn = angl;
                    }
                    else
                    {
                        move.SpeedUp = 0.0;
                        move.Turn = angl;
                    }
                }
            }
        }

        double ProtectZone(Point p)
        {
            Point protectPoint = GetMyGoalNetCenter() + new Vector(IsRivalsAtLeft ? -90 : 90, 0);
            double dist = protectPoint.GetDistanceTo(p);
            return 1 - 0.02 * dist;
        }

        Point AngleToProtectZone(Hockeyist self, Func<Point, double> field)
        {
            List<Point> points = GetPointsAround(self);
            Point dist = points[0];
            double distValue = double.MinValue;
            foreach (Point p in points)
            {
                double value = field(p);
                if (value > distValue)
                {
                    distValue = value;
                    dist = p;
                }
            }
            return dist;
        }
    }

    public class Attack : Strategy
    {
        const int swingMax = 19;
        const double strikeSpeed = 3.3;
        const double strikeFieldOk = -1.0;
        const double speedUpInStrikeZone = -1.0;
        const double alertZone = 200;

        public Attack(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            double strikeField = strikePointsField(Point.Get(self));
            double speed = GetSpead(self);
            if (InStrikeZone(self) || info.swingCount > 0)
            {
                AimAndStrike(move);
            }
            else
            {
                Point point = AngleToStrikeZone(self, world);
                double angl = self.GetAngleTo(point.X, point.Y);

                Vector myVector = new Vector(1, 0).Rotate(self.Angle);
                Vector toStrikeZone = new Vector(point.X - self.X, point.Y - self.Y);
                double projection = myVector * toStrikeZone / toStrikeZone.Abs();

                if (projection > 0.0)
                {
                    move.SpeedUp = 1.0;
                    move.Turn = angl;
                }
                else if (projection < -0.8)
                {
                    move.SpeedUp = -1.0;
                    move.Turn = angl;
                }
                else
                {
                    move.SpeedUp = 0.0;
                    move.Turn = angl;
                }

                if (strikeField > strikeFieldOk * 1.2 && angl < Math.PI / 70)
                    move.SpeedUp = speedUpInStrikeZone;

                if (self.LastAction.HasValue && self.LastAction.Value == ActionType.Swing)
                {
                    move.Action = ActionType.CancelStrike;
                    info.swingCount = 0;
                }
            }
        }

        protected void AimAndStrike(Move move)
        {
            Point goalPoint;

            Point goalNetLeft = new Point(world.GetOpponentPlayer().NetRight, world.GetOpponentPlayer().NetTop);
            Point goalNetRight = new Point(world.GetOpponentPlayer().NetRight, world.GetOpponentPlayer().NetTop + game.GoalNetHeight);

            Hockeyist goalie = GetGoalie(world.Hockeyists, world.GetMyPlayer());
            if (goalie != null)
            {
                double distTogoalNetLeft = goalie.GetDistanceTo(goalNetLeft.X, goalNetLeft.Y);
                double distTogoalNetRight = goalie.GetDistanceTo(goalNetRight.X, goalNetRight.Y);

                if (distTogoalNetLeft > distTogoalNetRight)
                    goalPoint = goalNetLeft;
                else
                    goalPoint = goalNetRight;
            }
            else
                goalPoint = GetRivalGoalNetCenter();

            double angl = self.GetAngleTo(goalPoint.X, goalPoint.Y);

            if (Math.Abs(angl) < Math.PI / 60 && GetSpead(self) > strikeSpeed)
            {
                move.Action = ActionType.Strike;
                info.swingCount = 0;
            }
            else if (Math.Abs(angl) < Math.PI / 60 || info.swingCount > 0)
            {
                if (info.swingCount < swingMax)
                {
                    move.Action = ActionType.Swing;
                    info.swingCount++;
                }
                else
                {
                    move.Action = ActionType.Strike;
                    info.swingCount = 0;
                }
            }
            else
            {
                info.swingCount = 0;
                move.Action = ActionType.CancelStrike;
                move.Turn = angl;
            }
        }

        bool InStrikeZone(Hockeyist self)
        {
            return strikePointsField(Point.Get(self)) > strikeFieldOk && (self.Y > world.GetOpponentPlayer().NetTop || self.Y < world.GetOpponentPlayer().NetBottom);
        }

        public override string Name { get { return "Attack"; } }

        double strikePointsField(Point point)
        {
            List<Point> points = GetStrikePoints();

            points.RemoveAll(x => { return x == Singleton.Instance.AmbushPoint; });

            double value = double.MinValue;

            foreach (Point p in points)
                value = Math.Max(1 - p.GetDistanceTo(point) * 0.02, value);

            return value;
            //List<Hockeyist> R = GetRival(info._world);
            //double topStrikeRivalCount = 1 - RivalCountOnPoint(R, topStrike) / R.Count;
            //double bottomStrikeRivalCount = 1 - RivalCountOnPoint(R, bottomStrike) / R.Count;

            //return Math.Max(1 - topStrike.GetDistanceTo(point) * 0.02 * topStrikeRivalCount, 1 - bottomStrike.GetDistanceTo(point) * 0.02);
        }

        int RivalCountOnPoint(List<Hockeyist> R, Point p)
        {
            int count = 0;
            foreach (Hockeyist h in R)
                if (p.GetDistanceTo(h) < alertZone)
                    count++;
            return count;
        }

        List<Hockeyist> GetRival(World world)
        {
            List<Hockeyist> list = new List<Hockeyist>();
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type != HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    list.Add(h);
                }
            return list;
        }

        Point AngleToStrikeZone(Hockeyist self, World world)
        {
            List<Point> points = GetPointsAround(self);
            Point dist = points[0];
            double distValue = double.MinValue;
            foreach (Point p in points)
            {
                double value = mainField(world, p, world.GetMyPlayer());
                if (value > distValue)
                {
                    distValue = value;
                    dist = p;
                }
            }
            return dist;
        }

        double mainField(World world, Point point, Player me)
        {
            return strikePointsField(point) + rivalsField(world.Hockeyists, point, me) + teamField(world.Hockeyists, point, me) + wallsField(point);
        }

        double teamField(Hockeyist[] hockeyists, Point point, Player me)
        {
            double field = 0;
            foreach (Hockeyist h in hockeyists)
                if (h.PlayerId == me.Id)
                {
                    double dis = h.GetDistanceTo(point.X, point.Y);
                    field += (-3 / (1 + Math.Exp(0.08 * dis - 4.5)));
                }
            return field;
        }

        double wallsField(Point point)
        {
            double field = 0;

            field = Math.Min(wallField(0, -1, 150, point), field);
            field = Math.Min(wallField(0, -1, 750, point), field);

            field = Math.Min(wallField(-1, 0, 65, point), field);
            field = Math.Min(wallField(-1, 0, 1135, point), field);

            return field;
        }
        double wallField(double a, double b, double c, Point point)
        {
            double dis = Math.Abs(a * point.X + b * point.Y + c) / Math.Sqrt(a * a + b * b);
            return -1 / (1 + Math.Exp(0.2 * dis - 5));//(-20 / dis) * (1 / (1 + Math.Exp(dis - 30)));
        }       

        Hockeyist GetGoalie(Hockeyist[] hockeyists, Player me)
        {
            foreach (Hockeyist h in hockeyists)
                if (h.Type == HockeyistType.Goalie && h.PlayerId != me.Id)
                    return h;
            return null;
        }
    }

    public class BrutalAttack : Attack
    {
        public override string Name { get { return "BrutalAttack"; } }

        public BrutalAttack(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            AimAndStrike(move);
        }
    }

    public class Raid : Strategy
    {
        public override string Name { get { return "Raid"; } }

        public Raid(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            Hockeyist rivalWithPuck = GetById(world.Puck.OwnerHockeyistId);
            if (game.StickLength > rivalWithPuck.GetDistanceTo(self))
            {
                double angl = self.GetAngleTo(rivalWithPuck);
                if (angl < Math.PI / 70)
                    move.Action = ActionType.Strike;
                else
                    move.Turn = angl;
            }
            else if (game.StickLength > self.GetDistanceTo(world.Puck))
            {
                double angl = self.GetAngleTo(world.Puck);
                if (angl < Math.PI / 70)
                    move.Action = ActionType.Strike;
                else
                    move.Turn = angl;
            }
            else
            {
                Hockeyist h = GetNearestTeam(rivalWithPuck);
                double x = rivalWithPuck.X + rivalWithPuck.SpeedX * 5;
                double y = rivalWithPuck.Y + rivalWithPuck.SpeedY * 5;
                move.Turn = self.GetAngleTo(x, y);
                if (h.Id == self.Id || center.GetDistanceTo(self) > 100)
                    move.SpeedUp = 1.0;
                else if (GetSpead(self) > 2)
                    move.SpeedUp = -1.0;
            }
        }
    }

    public class AttackAssistant : Strategy
    {
        public override string Name { get { return "AttackAssistant"; } }

        public AttackAssistant(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            Hockeyist rival;
            Hockeyist rivalGoalie = GetRivalGoalie(world);
            if (rivalGoalie != null)
            {
                Hockeyist rivalProt = GetNearestRival(world, rivalGoalie);
                if (rivalGoalie != null && rivalProt.GetDistanceTo(rivalGoalie) < 80)
                    rival = rivalProt;
                else
                    rival = GetNearestRival(world, GetById(world.Puck.OwnerHockeyistId));
            }
            else
                rival = GetNearestRival(world, GetById(world.Puck.OwnerHockeyistId));
            double angl = self.GetAngleTo(rival);
            if (angl < game.StrikeAngleDeviation && game.StickLength > rival.GetDistanceTo(self))
                move.Action = ActionType.Strike;
            else
                move.Turn = angl;
            move.SpeedUp = 1.0;
        }

        Hockeyist GetRivalGoalie(World world)
        {
            Player me = world.GetMyPlayer();
            foreach (Hockeyist h in world.Hockeyists)
                if (h.Type == HockeyistType.Goalie && h.PlayerId != me.Id)
                {
                    return h;
                }
            return null;
        }
    }

    public class Ambush : Strategy
    {
        public override string Name { get { return "Ambush"; } }

        public Ambush(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            double field = AmbushZone(GetNextCoordinates(self));
            if (field > 0.0)
            {
                if (GetSpead(self) > 1)
                    move.SpeedUp = 0.0;
                else
                {
                    move.Turn = self.GetAngleTo(Puck);
                    if (InActionZone(Puck))
                        move.Action = ActionType.TakePuck;
                }
            }
            else
            {
                Point point = AngleToZone(self, AmbushZone);
                Vector myVector = new Vector(1, 0).Rotate(self.Angle);
                Vector toProtectZone = new Vector(point.X - self.X, point.Y - self.Y);
                double projection = myVector * toProtectZone / toProtectZone.Abs();
                double angl = self.GetAngleTo(point.X, point.Y);

                if (projection > 0.0)
                {
                    move.SpeedUp = 1.0;
                    move.Turn = angl;
                }
                else if (projection < -0.8)
                {
                    move.SpeedUp = -1.0;
                    move.Turn = angl;
                }
                else
                {
                    move.SpeedUp = 0.0;
                    move.Turn = angl;
                }
            }
        }

        double AmbushZone(Point p)
        {
            if (Singleton.Instance.AmbushPoint == null)
                Singleton.Instance.AmbushPoint = Point.GetFarther(GetStrikePoints(), Point.Get(GetById(world.Puck.OwnerHockeyistId)));

            Point ambushPoint = Singleton.Instance.AmbushPoint;

            double dist = ambushPoint.GetDistanceTo(p);
            return 1 - 0.02 * dist + rivalsField(world.Hockeyists, p, world.GetMyPlayer());
        }

        protected override void OnEsc()
        {
            Singleton.Instance.AmbushPoint = null;
        }
    }

    public class Pass : Strategy
    {
        public override string Name { get { return "Pass"; } }

        public Pass(MyStrategy info) : base(info) { }

        public override void Run(Move move)
        {
            List<Hockeyist> team = GetMyTeam(world).FindAll((h) => { return h.Id != self.Id; });
            Hockeyist partner = team[0];
            double angl = self.GetAngleTo(partner);
            if (angl < game.PassAngleDeviation)
            {
                move.Action = ActionType.Pass;
                move.PassAngle = angl;
                move.PassPower = 1.5;
            }
            else
            {
                move.Turn = angl;
            }
        }
    }
}
