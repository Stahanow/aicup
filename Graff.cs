﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace GraffCore
{
    public partial class Graff<TNode, TKey>
        where TNode : class
        where TKey : class
    {
        Dictionary<TKey, TNode> nodeList;
        List<IEdge<TNode>> edges;
        Dictionary<TNode, List<IEdge<TNode>>> edgesDictionary;

        public int NodeCount { get { return nodeList.Count; } }
        public int EdgesCount { get { return edges.Count; } }
        public TNode this[TKey key] { get { return nodeList[key]; } }
        public IEnumerable<IEdge<TNode>> Edges { get { return edges; } }
        public IEnumerable<TNode> Nodes { get { return nodeList.Values; } }

        public Graff()
        {
            nodeList = new Dictionary<TKey, TNode>();
            edgesDictionary = new Dictionary<TNode, List<IEdge<TNode>>>();
            edges = new List<IEdge<TNode>>();
        }

        public void AddNode(TNode node, TKey key)
        {
            nodeList.Add(key, node);
        }
        public void RemoveNode(TNode node)
        {
            TKey key = GetKey(node);
            if (key != null)
                RemoveNode(key);
        }
        public void RemoveNode(TKey key)
        {
            TNode node = nodeList[key];
            if (node == null)
                return;

            if (edgesDictionary.ContainsKey(node))
            {
                List<IEdge<TNode>> edges = edgesDictionary[node];
                foreach (IEdge<TNode> edge in edges)
                    this.edges.Remove(edge);
            }

            edgesDictionary.Remove(node);

            nodeList.Remove(key);
        }
        public void AddEdge(TKey aKey, TKey bKey)
        {
            TNode aNode = this[aKey];
            TNode bNode = this[bKey];
            AddEdge(aNode, bNode);
        }
        public void AddEdge(TNode aNode, TNode bNode)
        {
            if (!nodeList.ContainsValue(aNode))
                throw new Exception("Graff does not contain " + aNode.ToString());

            if (!nodeList.Values.Contains<TNode>(bNode))
                throw new Exception("Graff does not contain " + bNode.ToString());

            IEdge<TNode> edge = new Edge<TNode>(aNode, bNode);
            GetEdgesList(aNode).Add(edge);
            GetEdgesList(bNode).Add(edge);
            edges.Add(edge);
        }
        public void AddEdge(IEdge<TNode> edge)
        {
            if (!nodeList.ContainsValue(edge.ANode))
                throw new Exception("Graff does not contain " + edge.ANode.ToString());

            if (!nodeList.Values.Contains<TNode>(edge.BNode))
                throw new Exception("Graff does not contain " + edge.BNode.ToString());

            //IEdge<TNode> edge = new Edge<TNode>(aNode, bNode);
            GetEdgesList(edge.ANode).Add(edge);
            GetEdgesList(edge.BNode).Add(edge);
            edges.Add(edge);
        }

        public void AddDirectedEdge(TNode start, TNode end)
        {
            if (!nodeList.ContainsValue(start))
                throw new Exception("Graff does not contain " + start.ToString());

            if (!nodeList.Values.Contains<TNode>(end))
                throw new Exception("Graff does not contain " + end.ToString());

            IEdge<TNode> edge = new OrientedEdge<TNode>(start, end);
            GetEdgesList(start).Add(edge);
            GetEdgesList(end).Add(edge);
            edges.Add(edge);
        }
        public void RemoveEdge(TNode bNode, TNode aNode)
        {
            if (!nodeList.ContainsValue(aNode) || !nodeList.ContainsValue(bNode))
                throw new Exception("Graff does not contain " + aNode.ToString());

            if (!nodeList.Values.Contains<TNode>(bNode))
                throw new Exception("Graff does not contain " + bNode.ToString());

            IEdge<TNode> edge = GetEdge(bNode, aNode);
            if (edge == null)
                return;
            edgesDictionary[bNode].Remove(edge);
            edgesDictionary[aNode].Remove(edge);
            edges.Remove(edge);
        }
        public bool HasPath(TNode bNode, TNode aNode)
        {
            return HasPath(bNode, aNode, new List<TNode>());
        }
        public bool IsConnected(TNode bNode, TNode aNode)
        {
            return GetNeighbors(bNode).Contains(aNode);
        }
        public List<TNode> GetNeighbors(TNode node)
        {
            return GetNodes(node);
        }
        public List<IEdge<TNode>> GetEdges(TNode node)
        {
            if (edgesDictionary.ContainsKey(node))
                return edgesDictionary[node];
            return null;
        }
        public TKey GetKey(TNode state)
        {
            if (!nodeList.ContainsValue(state))
                return null;

            foreach (TKey key in nodeList.Keys)
                if (this[key] == state)
                    return key;

            return null;
        }
        public bool Contains(TNode node)
        {
            return nodeList.Values.Contains(node);
        }

        bool HasPath(TNode bNode, TNode aNode, List<TNode> chekedNodes)
        {
            IEdge<TNode> edge = GetEdge(bNode, aNode);

            if (edge != null)
                return true;

            List<TNode> nodes = GetNodes(bNode);

            foreach (TNode node in chekedNodes)
                nodes.Remove(node);
            chekedNodes.Add(bNode);
            foreach (TNode node in nodes)
            {
                if (HasPath(node, aNode, chekedNodes))
                    return true;
            }
            return false;
        }
        List<TNode> GetNodes(TNode node)
        {
            List<TNode> nodes = new List<TNode>();
            List<IEdge<TNode>> edges = GetEdgesList(node);
            foreach (IEdge<TNode> edge in edges)
            {
                TNode neighbour = (TNode)edge[node];
                if (neighbour != null && edge.IsUnite(node, neighbour))
                    nodes.Add(neighbour);
            }
            return nodes;
        }
        List<IEdge<TNode>> GetEdgesList(TNode node)
        {
            if (edgesDictionary.ContainsKey(node))
                return edgesDictionary[node];

            List<IEdge<TNode>> list = new List<IEdge<TNode>>();
            edgesDictionary.Add(node, list);
            return list;
        }
        IEdge<TNode> GetEdge(TNode bNode, TNode aNode)
        {
            List<IEdge<TNode>> list;
            if (edgesDictionary.ContainsKey(bNode))
                list = edgesDictionary[bNode];
            else
                return null;

            foreach (IEdge<TNode> edge in list)
                if (edge.IsUnite(bNode, aNode))
                    return edge;
            return null;
        }
    }


    public class Graff<TKey> : Graff<INode, TKey> where TKey : class
    {
        public void AddNode(IKeyProvider<TKey> node)
        {
            base.AddNode(node, node.Key);
        }
    }
}
